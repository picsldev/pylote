# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2020 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient des variables et fonctions utiles au programme.
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys
import os



###########################################################"
#   VERSIONS DE PYTHON, QT, ETC
###########################################################"

# version de Python :
PYTHONVERSION = sys.version_info[0] * 10 + sys.version_info[1]

# PyQt5 ou PyQt4 :
PYQT = ''
if 'PYQT4' in sys.argv:
    try:
        import sip
        sip.setapi('QString', 2)
        sip.setapi('QVariant', 2)
        from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
        PYQT = 'PYQT4'
    except:
        pass
else:
    try:
        # on teste d'abord PyQt5 :
        from PyQt5 import QtCore, QtWidgets, QtGui
        PYQT = 'PYQT5'
    except:
        # puis PyQt4 :
        try:
            import sip
            sip.setapi('QString', 2)
            sip.setapi('QVariant', 2)
            from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
            PYQT = 'PYQT4'
        except:
            pass
if PYQT == '':
    print('YOU MUST INSTALL PYQT !')

# version de Qt :
qtVersion = QtCore.qVersion()

# détection du système (nom et 32 ou 64) :
OS_NAME = ['', '']
def detectPlatform():
    global OS_NAME
    # 32 ou 64 bits :
    if sys.maxsize > 2**32:
        bits = 64
    else:
        bits = 32
    # platform et osName :
    platform = sys.platform
    osName = ''
    if platform.startswith('linux'):
        osName = 'linux'
    elif platform.startswith('win'):
        osName = 'win'
    elif platform.startswith('freebsd'):
        osName = 'freebsd'
    elif platform.startswith('darwin'):
        import platform
        if 'powerpc' in platform.uname():
            osName = 'powerpc'
        else:
            osName = 'mac'
    OS_NAME = [osName, bits]
detectPlatform()

# TESTS affichera plus de messages :
TESTS = False
if 'TESTS' in sys.argv:
    TESTS = True
print('TESTS: ', TESTS, '| PYQT: ', PYQT)



###########################################################"
#   VARIABLES LIÉES AU LOGICIEL
###########################################################"

PROGNAME = 'pylote'
PROGLABEL = 'Pylote'
PROGVERSION = 2.1
PROGDATE = '2020/06/07'
HELPPAGE = 'http://pascal.peter.free.fr/pylote.html'



###########################################################"
#   DIVERS
###########################################################"

"""
La variable globale SCREEN_MODE définit la façon dont le 
logiciel occupe l'espace.
FULL_SCREEN : 
    tout l'écran est occupé, 
    y compris les barres de tâches et autres tableaux de bord.
FULL_SPACE : 
    l'espace libre est utilisé par le logiciel ; on peut 
    toujours utiliser le tableau de bord.
    Il peut y avoir des problèmes de positionnement sur certains systèmes.
TESTS : 
    le logiciel ne s'affiche que dans une petite fenêtre.
    utile juste pour voir les retours de la console en direct.
    À passer en argument.
"""
if OS_NAME[0] == 'win':
    SCREEN_MODE = 'FULL_SCREEN'
else:
    SCREEN_MODE = 'FULL_SPACE'
SCREEN_NUMBER = 0

def changeScreenMode(newValue):
    global SCREEN_MODE
    SCREEN_MODE = newValue

def changeScreenNumber(newValue):
    global SCREEN_NUMBER
    SCREEN_NUMBER = newValue


"""
La variable globale WITH_TRAY_ICON sera mis automatiquement à False 
si SCREEN_MODE vaut FULL_SCREEN où s'il y a problème.
On peut le changer ici manuellement 
si on n'en veut pas dans d'autres cas.
"""
WITH_TRAY_ICON = True

def changeWithTrayIcon(newValue):
    global WITH_TRAY_ICON
    WITH_TRAY_ICON = newValue



DEFAULTCONFIG = {
    'MAIN': {
        'attachDistance': 20, 
        'screenShotDelay': 1000, 
        'printMode': 'FullPage', 
        'printOrientation': 'Landscape', 
        'instrumentsScale': 1, 
        'unitsLocked': False, 
        }, 
    'TOLLBAR': {
        'first': True, 
        'masked': [], 
        'windowX': 120, 
        'windowY': 80, 
        }, 
    'KID': {
        'visible': ['FileReload', 'FileGoPrevious', 'FileGoNext'], 
        'windowX': 100, 
        'windowY': 200, 
        'windowHeight': 100, 
        'windowWidth': 250, 
        }, 
    'COLORS': {
        'CustomColor0': [0, 0, 0, 255], 
        'CustomColor1': [255, 255, 255, 255], 
        'CustomColor2': [255, 0, 0, 255], 
        'CustomColor3': [0, 255, 0, 255], 
        'CustomColor4': [0, 0, 255, 255], 
        'CustomColor5': [255, 255, 0, 255], 
        'CustomColor6': [0, 192, 192, 255], 
        'CustomColor7': [192, 0, 192, 255], 
        'CustomColor8': [255, 128, 0, 255], 
        'CustomColor9': [255, 0, 255, 255],         
        'actualColor': 'CustomColor3', 
        }, 
    'WIDTHS': {
        'CustomWidth0': 1, 
        'CustomWidth1': 5, 
        'CustomWidth2': 10, 
        'CustomWidth3': 20, 
        'CustomWidth4': 40, 
        'actualWidth': 'CustomWidth1', 
        }, 
    'FONT': {
        'family': '', 
        'pointSize': 14, 
        'color': [0, 0, 0, 255], 
        }, 
    'PEN': {
        'style': 'Solid', 
        }, 

    'LASTFILES': [], 

    'TEMP': {}, 
    }



STYLE = {}
def loadStyle():
    global STYLE
    style = QtWidgets.QApplication.style()
    STYLE = {
        'PM_ToolBarIconSize': style.pixelMetric(
            QtWidgets.QStyle.PM_ToolBarIconSize), 
        'PM_LargeIconSize': style.pixelMetric(
            QtWidgets.QStyle.PM_LargeIconSize), 
        'PM_SmallIconSize': style.pixelMetric(
            QtWidgets.QStyle.PM_SmallIconSize), 
        }
    #print(STYLE)

SUPPORTED_IMAGE_FORMATS = ('png',)
SUPPORTED_IMAGE_FORMATS_TEXT = '*.png *.jpg *.jpeg *.svg'

def loadSupportedImageFormats():
    global SUPPORTED_IMAGE_FORMATS
    SUPPORTED_IMAGE_FORMATS = QtGui.QImageReader.supportedImageFormats()
    #print(SUPPORTED_IMAGE_FORMATS)

def setSupportedImageFormatsText(newText):
    global SUPPORTED_IMAGE_FORMATS_TEXT
    SUPPORTED_IMAGE_FORMATS_TEXT = newText

def doIcon(fileName='', what='ICON'):

    inStyle = {
        #'application-exit': QtWidgets.QStyle.SP_TitleBarCloseButton, 
        'document-open': QtWidgets.QStyle.SP_DialogOpenButton, 
        'go-previous': QtWidgets.QStyle.SP_ArrowBack, 
        'view-refresh': QtWidgets.QStyle.SP_BrowserReload, 
        'go-next': QtWidgets.QStyle.SP_ArrowForward, 
        'go-down': QtWidgets.QStyle.SP_TitleBarMinButton, 
        'view-restore': QtWidgets.QStyle.SP_TitleBarNormalButton, 
        #'help-contents': QtWidgets.QStyle.SP_DialogHelpButton, 
        }
    if fileName in inStyle:
        return QtWidgets.QApplication.style().standardIcon(inStyle[fileName])

    ext = ''
    if len(fileName.split('.')) > 1:
        ext = fileName.split('.')[1]
        fileName = fileName.split('.')[0]

    if ext == '':
        for test in ('png', 'svgz'):
            if test in SUPPORTED_IMAGE_FORMATS:
                allFileName = 'images/{0}.{1}'.format(fileName, test)
                if QtCore.QFile(allFileName).exists():
                    ext = test
        allFileName = 'images/{0}.{1}'.format(fileName, ext)
        if ext == 'png':
            print('doIcon:', fileName)
    else:
        if ext in SUPPORTED_IMAGE_FORMATS:
            allFileName = 'images/{0}.{1}'.format(fileName, ext)
        else:
            allFileName = 'images/{0}.png'.format(fileName)
        if not(QtCore.QFile(allFileName).exists()):
            print('doIcon:', allFileName)
            allFileName = 'images/{0}.png'.format(fileName)
    if what == 'ICON':
        #return QtGui.QIcon(allFileName)
        #return QtGui.QIcon.fromTheme(fileName, QtGui.QIcon('images/aaa.png'))
        return QtGui.QIcon.fromTheme(fileName, QtGui.QIcon(allFileName))
    else:
        return QtGui.QPixmap(allFileName)



FLAGS = {
    'selectable': QtWidgets.QGraphicsItem.ItemIsSelectable, 
    'movable': QtWidgets.QGraphicsItem.ItemIsSelectable | 
        QtWidgets.QGraphicsItem.ItemIsMovable, 
    'ignoresTransformations': QtWidgets.QGraphicsItem.ItemIgnoresTransformations, 
    #'': , 
    }

def doFlags(what, flag):
    if flag in FLAGS:
        what.setFlags(FLAGS[flag])
    elif flag == 'locked':
        what.setFlags(FLAGS['selectable'])
        what.setFlag(
            FLAGS['selectable'],
            enabled=False)


